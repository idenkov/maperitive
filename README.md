# Maperitive Ruleset


These project aims to document some custom rulesets for the program [Maperitive](https://maperitive.net). 
Maperitive is a program to draw maps with Openstreetmap- or GPS data using custom rulesets.

## Examples

![Example picture Tokyo with the water rendered white](img/Tokyo-whitewater.png "Tokyo-whitewater")*Tokyo with water rendered in white*
![Example picture Tokyo with water rendered lightgray](img/Tokyo.png "Tokyo")*Tokyo*
![Example picture Stockholm](img/Stockholm.png "Stockholm")*Stockholm*
![Example picture Berlin](img/Berlin.png "Berlin")*Berlin*


## How to use the rulesets

1. Download example.mrules file or insert the documents text into an example.txt file and rename it example.mrules. Standard path for the Maperitive rules is `Maperitive/rules`. 

2. Load some OSM data into Maperitive using the build-in download function. You can restrict the map data by setting the geometry boarders in Maperitive beforehand.

3. Load the ruleset using `use-ruleset /pathtofile/example.mrules` and `apply-ruleset` with the programs console.

4. Make sure you have deactivated the online view of the osm in the bottom right corner (star is grey).


## Comments 

- To get rid of the watermark you can export the SVG file and delete/ hide the responding layers. Works flawlessly with the free and open source software 'Inkscape'.

## Known issues

- If your geometry boarder cuts off a big object like a lake, the download function might not save the object at all. The object would not be rendered in your map even though the ruleset includes this object. Same with boarders etc. 